package zhangxinhua.zhongqiugushi;


/**
 * TODO
 *
 * @author zxhtom
 * 2022/9/5
 */
public class MoonDemo {
    static class Moon{

        public Moon have(String what_time) {
            System.out.println("what time have moon");
            return this;
        }
    }

    static class Sky {

    }
    static class wine{

        public void askSky(Sky sky) {
            System.out.println("把酒问青天");
        }
    }
    public static void main(String[] args) {
        Moon moon = new Moon();
        moon.have("what time");
        wine wine = new wine();
        wine.askSky(new Sky());
    }
}
