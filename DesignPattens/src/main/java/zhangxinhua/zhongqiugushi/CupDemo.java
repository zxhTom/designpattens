package zhangxinhua.zhongqiugushi;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/9/5
 */
public class CupDemo {
    static class Relation {
        Cup cup;
        Moon moon;

        public Relation(Cup cup, Moon moon) {
            this.cup = cup;
            this.moon = moon;
        }

        public String shadow() {
            return "three people";
        }

    }
    static class Cup{
        public Cup() {

        }
        public Cup up() {
            System.out.println("举杯");
            return this;
        }
    }
    static class Moon{
        public Moon() {

        }
    }
    public static void main(String[] args) {
        Relation relation = new Relation(new Cup(), new Moon());
        System.out.println(relation.shadow());
    }
}
