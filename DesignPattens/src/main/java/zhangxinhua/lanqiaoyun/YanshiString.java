package zhangxinhua.lanqiaoyun;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/6/16
 */
public class YanshiString {
    public static void main(String[] args) {
        String text = "abc";
        String pre = "123";
        String result = new String(text+pre);
        System.out.println("第一次的[abc123]内存地址:"+System.identityHashCode(result));
        result.intern();//add
        result = "new";
        System.out.println("中途离开了："+System.identityHashCode(result));
        String newResult = text + pre;
        newResult = newResult.intern();//add
        System.out.println("原始:"+System.identityHashCode(newResult));
    }

    public static int hash(String text) {
        return text.hashCode();
    }
}
