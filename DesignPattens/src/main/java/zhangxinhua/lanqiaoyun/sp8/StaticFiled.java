package zhangxinhua.lanqiaoyun.sp8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author zxhtom
 */
public class StaticFiled {
    private static int result = 0;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result += result;
    }

    public static void main(String[] args) {
        NumberAdd add = new NumberAdd();
        //开始之前，已经有采集队使用程序计算分析采集数据了，他们通过程序进行了简单的设置
        StaticFiled filed = new StaticFiled();
        filed.setResult(100);
        StaticFiled filed1 = new StaticFiled();
        //[[-4, -3, 7], [-4, -2, 6], [-4, 0, 4], [-3, 1, 2], [-2, 0, 2]]
        Integer[] arr1 = new Integer[]{4, 2, 6, 7, 1, -2, 0, -4, -3};
        List<Integer> list1 = Arrays.asList(arr1);
        //[[-4, -3, 7], [-4, 1, 3], [-3, 0, 3], [-3, 1, 2], [-2, 0, 2]]
        Integer[] arr2 = new Integer[]{3, 2, 3, 7, 1, -2, 0, -4, -3};
        List<Integer> list2 = Arrays.asList(arr2);
        List<List<Integer>> lists1 = add.threeSum(list1);
        List<List<Integer>> lists2 = add.threeSum(list2);
        filed1.setResult(lists1.size());
        filed1.setResult(lists2.size());
        // should 10
        System.out.println("一共拥有："+filed1.getResult()+"方案");
        String format = String.format("你的统计分别为：%s ; %s\n", lists1, lists2);
        System.out.println("你的统计方案如下："+format);
    }
}
