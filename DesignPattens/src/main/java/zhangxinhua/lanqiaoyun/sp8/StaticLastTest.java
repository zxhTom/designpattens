package zhangxinhua.lanqiaoyun.sp8;

import java.util.*;

/**
 * @author zxhtom
 */
public class StaticLastTest {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Integer[] arr1 = ThreeZeroArrCreateUtils.getInstance().create();
            List<Integer> list1 = Arrays.asList(arr1);
            Integer[] arr2 = ThreeZeroArrCreateUtils.getInstance().create();
            List<Integer> list2 = Arrays.asList(arr2);
            NumberAdd add = new NumberAdd();
            List<List<Integer>> lists1 = add.threeSum(list1);
            List<List<Integer>> lists2 = add.threeSum(list2);
            NumberAddAns addans = new NumberAddAns();
            List<List<Integer>> listsans1 = addans.threeSum(list1);
            List<List<Integer>> listsans2 = addans.threeSum(list2);
            StaticFiled filed1 = new StaticFiled();
            StaticFiledAns filed2 = new StaticFiledAns();
            filed1.setResult(lists1.size());
            filed1.setResult(lists2.size());
            filed2.setResult(listsans1.size());
            filed2.setResult(listsans2.size());
            String format = String.format("数组分别：%s ; %s\n", Arrays.toString(arr1), Arrays.toString(arr2));
            format += String.format("你的方案数：%s;\n你的统计分别为：%s ; %s\n",filed1.getResult(), lists1, lists2);
            format += String.format("正确的方案数：%s;\n正确应分别为：%s ; %s\n",filed2.getResult(), listsans1, listsans2);
            if (!equals(lists1,listsans1) || !equals(lists2,listsans2)) {
                throw new RuntimeException("好像有方案不正确\n"+format);
            }
            if (filed1.getResult() != filed2.getResult()) {
                throw new RuntimeException("方案计算数量不匹配\n"+format);
            }
        }
    }

    private static boolean equals(List<List<Integer>> sourceList, List<List<Integer>> targetList) {
        if (sourceList.size() != targetList.size()) {
            return false;
        }
        Set<String> sourceSet = extracted(sourceList);
        Set<String> targetSet = extracted(targetList);

        Iterator<String> iterator = sourceSet.iterator();
        while (iterator.hasNext()) {
            if (!targetSet.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }

    private static Set<String> extracted(List<List<Integer>> sourceList) {
        Set<String> sourceSet = new HashSet<>();
        for (List<Integer> source : sourceList) {
            String res = "";
            for (Integer integer : source) {
                res += integer;
            }
            sourceSet.add(res);
        }
        return sourceSet;
    }


}
