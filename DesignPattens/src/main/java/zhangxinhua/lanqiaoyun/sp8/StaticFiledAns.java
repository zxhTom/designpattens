package zhangxinhua.lanqiaoyun.sp8;

import java.util.Arrays;
import java.util.List;

/**
 * @author zxhtom
 */
public class StaticFiledAns {
    private int result = 0;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result += result;
    }

}
