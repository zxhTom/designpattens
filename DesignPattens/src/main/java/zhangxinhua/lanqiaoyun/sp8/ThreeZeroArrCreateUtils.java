package zhangxinhua.lanqiaoyun.sp8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author zxhtom
 */
public class ThreeZeroArrCreateUtils {
    private static ThreeZeroArrCreateUtils util = new ThreeZeroArrCreateUtils();
    private ThreeZeroArrCreateUtils() {

    }

    public static ThreeZeroArrCreateUtils getInstance() {
        return util;
    }

    public Integer[] create() {
        Random random = new Random();
        Integer size = random.nextInt(10) + 1;
        Integer[] arr = new Integer[size+3];
        Map<Integer, Integer> countMap = new HashMap<>();
        Integer sameSize = 2;
        Integer index = 0;
        int r1 = random.nextInt(100);
        int r2 = random.nextInt(100);
        Integer[] first = new Integer[]{r1, r2, 0-(r1+r2)};
        System.arraycopy(first, 0, arr, size, 3);
        while (index < size) {
            int i = random.nextInt(20);
            int j = random.nextInt(40);
            int num = i - j;
            if (countMap.containsKey(num)) {
                if (Integer.valueOf(countMap.get(num)) > sameSize) {
                    continue;
                }
            }
            countMap.put(num, countMap.getOrDefault(num, 0)+1);
            arr[index++] = num;
        }
        return arr;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(ThreeZeroArrCreateUtils.getInstance().create()));
    }
}
