package zhangxinhua.lanqiaoyun.sp2;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/27
 */
public class EqualTestAns {
    public static void main(String[] args) {
        String params = args[0];
        String[] split = params.split(",");
        String firstPeople = split[0];
        String firstFav=split[1];
        boolean firstPeopleToken = Boolean.parseBoolean(split[2]);
        String secondPeople = split[3];
        String secondFav = split[4];
        boolean secondPeopleToken = Boolean.parseBoolean(split[5]);
        ClassEqualAns equal1 = new ClassEqualAns();
        equal1.setName(firstPeople);
        equal1.setFav(firstFav);
        equal1.setAll(firstPeopleToken);
        ClassEqualAns equal2 = new ClassEqualAns();
        equal2.setName(secondPeople);
        equal2.setFav(secondFav);
        equal2.setAll(secondPeopleToken);
        System.out.println(equal1.equals(equal2));
    }
}
