package zhangxinhua.lanqiaoyun.sp2.v002;

import java.util.Objects;

/**
 * 人员属性定义处
 */
class V002People{
    //用于描述个人行为
    private String fav;

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        V002People that = (V002People) o;
        return Objects.equals(fav, that.fav);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fav);
    }
}
