package zhangxinhua.lanqiaoyun.sp2.v002;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private static FileUtils utils = new FileUtils();

    private FileUtils() {

    }

    public static FileUtils getInstance() {
        return utils;
    }

    public List<String> lineList(String fileName) {
        List<String> result = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
