package zhangxinhua.lanqiaoyun.sp2.v002;

import lombok.Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/8/8
 */
public class BigestAttr {
    public static void main(String[] args) {
        //定义你心中完美意中人行为属性
        V002People lover = new V002People();
        lover.setFav("aaa");
        //生成考察者行为属性
        V002People waiter = new V002People();
        waiter.setFav("lanqiaoyunazxhaaa");
        //判断考察者是否符合你的意中人要求
        System.out.println(lover.equals(waiter));
        BigestAttr item = new BigestAttr();
        System.out.println(item.longestPalindromeSubseq("ab"));
        List<String> strings = FileUtils.getInstance().lineList("/Users/zxhtom/zxh/project/git/designpattens/DesignPattens/src/main/java/zhangxinhua/lanqiaoyun/sp2/v002/unitTests.txt");
        for (String string : strings) {
            String[] s = string.split(" ");
            System.out.println("s = " + s);
        }
    }

    public String longestPalindromeSubseq(String s) {
        int n = s.length();
        String[][] dp = new String[n][n];
        for (int i = n - 1; i >= 0; i--) {
            dp[i][i] = String.valueOf(s.charAt(i));
            char c1 = s.charAt(i);
            for (int j = i + 1; j < n; j++) {
                char c2 = s.charAt(j);
                if (c1 == c2) {
                    dp[i][j] = c1+dp[i+1][j-1]+c2;
                } else {
                    if(dp[i+1][j].length()>dp[i][j-1].length()){
                        dp[i][j] = dp[i+1][j];
                    }else{
                        dp[i][j]= dp[i][j-1];
                    }
                }
            }
        }
        return dp[0][n - 1];
    }


    @Override
    public String toString() {
        return "BigestAttr{}";
    }
}
