package zhangxinhua.lanqiaoyun.sp2;

import java.util.Objects;

public class ClassEqualAns {
    private String name;
    private String fav;
    private boolean all;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassEqualAns obj = (ClassEqualAns) o;
        if (this.fav.equals(obj.getFav())) {
            return true;
        }
        if (this.all || obj.isAll()) {
            return true;
        }
        String prefix = "喜结连理";
        if (prefix.equals(this.name) || prefix.equals(obj.getName())) {
            return true;
        }
        ClassEqualAns that = (ClassEqualAns) o;
        return all == that.all && Objects.equals(this.name, that.name) && Objects.equals(fav, that.fav);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fav, all);
    }
}
