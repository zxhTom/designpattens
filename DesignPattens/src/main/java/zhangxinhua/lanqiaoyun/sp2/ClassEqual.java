package zhangxinhua.lanqiaoyun.sp2;

import java.util.Objects;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/27
 */
public class ClassEqual {
    private String name;
    private String fav;
    private boolean all;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }
}
