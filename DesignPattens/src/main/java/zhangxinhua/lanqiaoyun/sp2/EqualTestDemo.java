package zhangxinhua.lanqiaoyun.sp2;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/27
 */
public class EqualTestDemo {
    public static void main(String[] args) {
        String firstPeople = "zhangsan";
        String firstFav = "打篮球";
        boolean firstAll = false;
        String secondPeople = "lisi";
        String secondFav = "打篮球";
        boolean secondAll = false;
        ClassEqual equal1 = new ClassEqual();
        equal1.setName(firstPeople);
        equal1.setFav(firstFav);
        equal1.setAll(firstAll);
        ClassEqual equal2 = new ClassEqual();
        equal2.setName(secondPeople);
        equal2.setFav(secondFav);
        equal2.setAll(secondAll);
        System.out.println(equal1.equals(equal2));
    }
}
