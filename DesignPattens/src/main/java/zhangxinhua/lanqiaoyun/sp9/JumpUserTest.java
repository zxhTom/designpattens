package zhangxinhua.lanqiaoyun.sp9;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author zxhtom
 */
public class JumpUserTest {
    public static void main(String[] args) {
        int[] stepArr = new int[]{3, 2, 1, 5, 3, 4, 2, 1, 3, 4, 2};
        MinestStep minestStep = new MinestStep();
        //测试并发时，可以调大此参数
        Integer member = 20;
        CountDownLatch latch = new CountDownLatch(member);
        Solution solution = new Solution();
        List<Thread> threadList = new ArrayList<>();
        System.out.print("家庭挑战结果：");
        for (int i = 0; i < member; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(80);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    solution.addSolution(minestStep, stepArr);
                    latch.countDown();
                }
            });
            threadList.add(thread);
        }
        for (Thread thread : threadList) {
            thread.start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
    }
}
