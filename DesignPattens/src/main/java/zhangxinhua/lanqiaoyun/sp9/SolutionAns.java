package zhangxinhua.lanqiaoyun.sp9;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxhtom
 */
public class SolutionAns {
    public volatile List<Integer> soluation = new ArrayList<>();
    public void addSolution(StepInterface step , int[] stepArr) {
        int jump = step.jump(stepArr);
        synchronized (this) {
            if (soluation.size() == 0) {
                soluation.add(jump);
            }
        }
    }

}
