package zhangxinhua.lanqiaoyun.sp9;

import java.util.concurrent.CountDownLatch;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/8/27
 */
public class JumpRunnable implements Runnable {
    private CountDownLatch latch;
    private Solution solution;
    private MinestStep minestStep;
    private int[] stepArr;
    public JumpRunnable(CountDownLatch latch,Solution solution , MinestStep minestStep,int[] stepArr) {
        this.latch = latch;
        this.solution = solution;
        this.minestStep = minestStep;
        this.stepArr = stepArr;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(80);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        solution.addSolution(minestStep, stepArr);
        latch.countDown();
    }
}
