package zhangxinhua.lanqiaoyun.sp9;

import java.util.Random;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/8/24
 */
public class StepUtilCreate {
    private static StepUtilCreate create = new StepUtilCreate();

    private StepUtilCreate() {

    }

    public static StepUtilCreate getInstance() {
        return create;
    }

    public int[] create() {
        Random random = new Random();
        int size = random.nextInt(10) + 2;
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            int step = random.nextInt(5) + 1;
            arr[i] = step;
        }
        return arr;
    }
}
