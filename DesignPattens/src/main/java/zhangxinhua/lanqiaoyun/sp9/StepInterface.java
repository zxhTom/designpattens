package zhangxinhua.lanqiaoyun.sp9;

/**
 * @author zxhtom
 */
public interface StepInterface {
    public int jump(int[] nums);
}
