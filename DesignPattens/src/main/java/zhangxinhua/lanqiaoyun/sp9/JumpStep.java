package zhangxinhua.lanqiaoyun.sp9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author zxhtom
 */
public class JumpStep {


    public static void main(String[] args) throws InterruptedException {


        //生成阶梯可跨方案
        int[] stepArr = StepUtilCreate.getInstance().create();
        MinestStep minestStep = new MinestStep();
        MinestStepAns minestStepAns = new MinestStepAns();
        Integer testNum = 10;
        int answer = minestStepAns.jump(stepArr);
        for (Integer tn = 0; tn < testNum; tn++) {
            List<Thread> threadList = new ArrayList<>();
            Solution solution = new Solution();
            int threadNum = 20;
            CountDownLatch latch = new CountDownLatch(threadNum);
            for (int i = 0; i < threadNum; i++) {
                int finalI = i;
                JumpRunnable jumpRunnable = new JumpRunnable(latch, solution, minestStep, stepArr);
                Thread thread = new Thread(jumpRunnable);
                threadList.add(thread);
            }

            for (Thread thread : threadList) {
                thread.start();
            }
            latch.await();
            String s = Arrays.toString(stepArr);
            if (solution.soluation.size() > 1) {
                throw new RuntimeException("貌似你的计算重复了:地图数据:"+s);
            }
            for (Integer cin :solution.soluation){
                if (null == cin) {
                    throw new RuntimeException("为什么你的计算有null值:地图数据:"+s);
                }
                if (answer != cin) {
                    throw new RuntimeException("检测到你有错误数据:"+cin+":地图数据:"+s);
                }
            }
        }
    }
}
