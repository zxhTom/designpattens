package zhangxinhua.lanqiaoyun.sp4;

/**
 * @author zxhtom
 */
public class BusinessMan {
    //商人给出的单价
    private int radio;

    public BusinessMan(int radio) {
        if (radio < 1) {
            throw new RuntimeException("系统检测到商家出价超出了常理...");
        }
        this.radio = radio;
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }
}
