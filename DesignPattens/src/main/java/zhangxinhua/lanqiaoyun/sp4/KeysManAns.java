package zhangxinhua.lanqiaoyun.sp4;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author zxhtom
 */
public class KeysManAns {
    public static void main(String[] args) {
        for (int k = 0; k < 10; k++) {
            KeysManAns manAns = new KeysManAns();
            KeysMan man = new KeysMan();
            Random random = new Random();
            int levelNum = random.nextInt(20)+1;
            int currentTotal = random.nextInt(100)+1;
//        int levelNum = 3;
            int maxLimit=1000000;
            int manBaoshi = man.climbStairs(levelNum);
            int manAnsBaoshi = manAns.climbStairs(levelNum);
            FinalCalc calc = new FinalCalc(currentTotal, manBaoshi,maxLimit);
            FinalCalc calcAns = new FinalCalc(currentTotal,manAnsBaoshi,maxLimit);
            List<BusinessMan> manList = new ArrayList<BusinessMan>();
            for (int i = 0; i < 3; i++) {
                manList.add(new BusinessMan(random.nextInt(100)+1));
                manList.add(new BusinessMan(random.nextInt(100)+1));
                manList.add(new BusinessMan(random.nextInt(100)+1));
            }
            int max = 0;
            for (BusinessMan businessMan : manList) {
                int total = man.handlerPrice(businessMan, calc);
                if (max < total) {
                    max = total;
                }
            }
            int maxAns = 0;
            for (BusinessMan businessMan : manList) {
                int total = manAns.handlerPrice(businessMan, calcAns);
                if (maxAns < total) {
                    maxAns = total;
                }
            }
            List<Integer> radioList = manList.stream().map(BusinessMan::getRadio).collect(Collectors.toList());
            String format = String.format("\n阶梯：%s , 宝石：%s ,账户已有余额：%s , 商人们出价:%s,平台账户总限额：%s\n", levelNum,manAnsBaoshi,currentTotal, radioList.toString(),maxLimit);
            String result = String.format("正确答案：%s , 你的答案 : %s", maxAns, max);
            if (max < maxAns) {
                throw new RuntimeException(format+"还有性价比更高的商人哦，继续努力吧。\n"+result);
            } else if (max > maxAns) {
                throw new RuntimeException(format+"请原谅我并未找到你所说的方案，是不是忘记平台余额限制啦？\n"+result);
            }
        }
    }


    public int climbStairs(int n) {
        int[] total = new int[n];
        total[0] = 1;
        for (int i = 1; i < n; i++) {
            if (i > 1) {
                total[i] = total[i - 1] + total[i - 2];
            } else {
                total[i] = total[i - 1] + 1;
            }
        }
        return total[n-1];
    }
    public int handlerPrice(BusinessMan businessMan , FinalCalc finalCalc) {
        FinalCalc tempCalc = new FinalCalc();
        try {
            Class<? extends FinalCalc> clazz = finalCalc.getClass();
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                Object o = declaredField.get(finalCalc);
                declaredField.set(tempCalc, o);
            }
        } catch (Exception e) {
            throw new RuntimeException("服务脚本检测逻辑出错，请联系作者尽快修改");
        }
        int money = businessMan.getRadio() * tempCalc.getLqBitNum();
        tempCalc.setTotal(tempCalc.getTotal()+money);
        return tempCalc.canDo() ? tempCalc.getTotal() : -1;
    }
}
