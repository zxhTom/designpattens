package zhangxinhua.lanqiaoyun.sp4;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxhtom
 */
public class KeysMan {
    public static void main(String[] args) {
        KeysMan main = new KeysMan();
        //台阶数量
        int levelNum = 20;
        int actionNum = main.climbStairs(levelNum);
        int currentTotal = 0;
        FinalCalc calc = new FinalCalc(currentTotal, actionNum, 1000000);
        //市场中愿意出价的商人群体
        List<BusinessMan> manList = new ArrayList<BusinessMan>();
        for (int i = 0; i < 3; i++) {
            manList.add(new BusinessMan(2));
            manList.add(new BusinessMan(5));
            manList.add(new BusinessMan(3));
        }
        int max = 0;
        //获取满足平台交易条件的最高价
        for (BusinessMan businessMan : manList) {
            int total = main.handlerPrice(businessMan, calc);
            if (max < total) {
                max = total;
            }
        }
        System.out.println("获取宝石数量：" + actionNum);
        System.out.println("商人出价最高：" + 5);
        System.out.println("你的账户最终余额：" + currentTotal+actionNum*5);
        System.out.println("你算出来最终余额：" + max);
    }

    public int climbStairs(int nums) {
        //TODO 请你完成计算探测方案数目
        return 1;
    }

    public int handlerPrice(BusinessMan businessMan , FinalCalc finalCalc) {
        //TODO 请你预测本次交易后账户平台余额
        int money = businessMan.getRadio() * finalCalc.getLqBitNum();
        finalCalc.setTotal(finalCalc.getTotal()+money);
        return finalCalc.canDo() ? finalCalc.getTotal() : -1;
    }
}
