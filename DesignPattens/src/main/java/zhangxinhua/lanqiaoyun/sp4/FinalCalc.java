package zhangxinhua.lanqiaoyun.sp4;

/**
 * @author zxhtom
 */
public final class FinalCalc {
    //全部交易金额
    private int total;
    //宝石数量
    private int lqBitNum;
    //平台账户账户余额上限
    private int maxLimit;

    public FinalCalc() {
    }
    public FinalCalc(int total, int lqBitNum, int maxLimit) {
        this.total = total;
        this.lqBitNum = lqBitNum;
        this.maxLimit = maxLimit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getLqBitNum() {
        return lqBitNum;
    }

    /**
     * 用于检测平台账户是否能接收此笔费用
     */
    public boolean canDo() {
        return this.total <= this.maxLimit;
    }
}
