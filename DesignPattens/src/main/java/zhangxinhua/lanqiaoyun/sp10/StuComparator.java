package zhangxinhua.lanqiaoyun.sp10;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * @author zxhtom
 */
public class StuComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        Double score = o1.getScore();
        Double score2 = o2.getScore();
        BigDecimal b1 = new BigDecimal(score.toString());
        BigDecimal b2 = new BigDecimal(score2.toString());
        if (b1.subtract(b2).intValue()==0) {
            BigDecimal s1 = new BigDecimal(o1.getStuNo());
            BigDecimal s2 = new BigDecimal(o2.getStuNo());
            return s1.subtract(s2).intValue();
        }
        return b1.subtract(b2).intValue();
    }
}
