package zhangxinhua.lanqiaoyun.sp10;

import java.text.DecimalFormat;

/**
 * @author zxhtom
 */
public class Student {
    private int stuNo;
    private String name;
    private Double score;
    DecimalFormat df = new DecimalFormat("#.00");
    public int getStuNo() {
        return stuNo;
    }

    public void setStuNo(int stuNo) {
        this.stuNo = stuNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getScore() {
        return score;
    }

    //保证分数是两位小数格式
    public void setScore(Double score) {
        this.score = Double.valueOf(df.format(score));
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuNo=" + stuNo +
                ", name='" + name + '\'' +
                ", score=" + score +
                "}\n";
    }
}
