package zhangxinhua.lanqiaoyun.sp10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zxhtom
 */
public class ALlLineAns{
    public static void main(String[] args) {
        Integer[] arr = new Integer[]{1,2,3};
        ALlLineAns aLlLineAns = new ALlLineAns();
        System.out.println(aLlLineAns.permute(arr));
    }
    public int permute(Integer[] nums) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();

        List<Integer> output = new ArrayList<Integer>();
        for (int num : nums) {
            output.add(num);
        }

        int n = nums.length;
        backtrack(n, output, res, 0);
        return res.size();
    }

    public void sorted(List<Student> studentList) {
        List<Student> collect = studentList.stream().sorted(new StuComparator()).collect(Collectors.toList());
        studentList.clear();
        studentList.addAll(collect);
    }

    public void backtrack(int n, List<Integer> output, List<List<Integer>> res, int first) {
        // 所有数都填完了
        if (first == n) {
            res.add(new ArrayList<Integer>(output));
        }
        for (int i = first; i < n; i++) {
            // 动态维护数组
            Collections.swap(output, first, i);
            // 继续递归填下一个数
            backtrack(n, output, res, first + 1);
            // 撤销操作
            Collections.swap(output, first, i);
        }
    }
}
