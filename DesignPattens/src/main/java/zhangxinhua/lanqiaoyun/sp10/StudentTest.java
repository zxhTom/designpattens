package zhangxinhua.lanqiaoyun.sp10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zxhtom
 */
public class StudentTest {
    public static void main(String[] args) {
        List<Student> studentList = StudentUtilCreate.getInstance().create();
        AllLine allLine = new AllLine();
        ALlLineAns aLlLineAns = new ALlLineAns();
        for (Student student : studentList) {
            int stuNo = student.getStuNo();
            Set<Integer> set = new HashSet<>();
            while (stuNo != 0) {
                set.add(stuNo % 10);
                stuNo /= 10;
            }
            Integer[] integers = set.toArray(new Integer[set.size()]);
            int permute = allLine.permute(integers);
            int permuteAns = aLlLineAns.permute(integers);
            if (permute != permuteAns) {
                throw new RuntimeException("计算学号基础分出错.....");
            }
            BigDecimal b1 = new BigDecimal(student.getScore());
            BigDecimal b2 = new BigDecimal(Double.valueOf(permuteAns));
            student.setScore(b1.add(b2).doubleValue());
        }
        List<Student> ansStudentList = new ArrayList<>();
        ansStudentList.addAll(studentList);
        aLlLineAns.sorted(ansStudentList);
        List<String> collectAns = ansStudentList.stream().map(Student::getName).collect(Collectors.toList());
        allLine.sorted(studentList);
        List<String> collect = studentList.stream().map(Student::getName).collect(Collectors.toList());
        System.out.println("正确排序后学生列表：\n" + ansStudentList);
        System.out.println("你的排序后学生列表：\n" + studentList);
        if (collect.size() != collectAns.size()) {
            throw new RuntimeException("学生人数不对....");
        }
        for (int i = 0; i < collectAns.size(); i++) {
            String stuNameAns = collectAns.get(i);
            String stuName = collect.get(i);
            if (!stuName.equals(stuNameAns)) {
                throw new RuntimeException(String.format("第%s为学生好像并不是[%s],我认为应该是[%s]", i+1, stuName, stuNameAns));
            }
        }
    }
}
