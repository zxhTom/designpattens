package zhangxinhua.lanqiaoyun.sp10;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author zxhtom
 */
public class StudentUtilCreate {
    private static StudentUtilCreate create = new StudentUtilCreate();

    private StudentUtilCreate() {

    }
    public static StudentUtilCreate getInstance() {
        return create;
    }

    //获取学生列表
    public List<Student> create() {
        List<Student> stuList = new ArrayList<>();
        Random random = new Random();
        List<Integer> stuNoList = new ArrayList<>();
        int size = random.nextInt(10) + 5;
        for (int i = 0; i < size; i++) {
            Student student = new Student();
            student.setName("学生"+(i+1));
            int stuNo = random.nextInt(1000) + 1000;
            while (stuNoList.contains(stuNo)) {
                stuNo = random.nextInt(1000) + 1000;
            }
            stuNoList.add(stuNo);
            student.setStuNo(stuNo);
            DecimalFormat df = new DecimalFormat("#.00");
            student.setScore(Double.valueOf(df.format(random.nextDouble()*100)));
            stuList.add(student);
        }
        System.out.println("原始学生列表： \n"+stuList);
        return stuList;
    }

}
