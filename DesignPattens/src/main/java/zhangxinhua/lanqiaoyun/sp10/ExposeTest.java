package zhangxinhua.lanqiaoyun.sp10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zxhtom
 */
public class ExposeTest {
    public static void main(String[] args) {
        AllLine allLine = new AllLine();
        List<Student> studentList = new ArrayList<>();

        Student student1 = new Student();
        student1.setStuNo(1234);
        student1.setScore(15.62D);
        student1.setName("孙悟空");
        //student1 学号1234对应平时分24 ，总分=39.62
        Student student2 = new Student();
        student2.setStuNo(123);
        student2.setScore(16.16D);
        student2.setName("猪八戒");
        //student1 学号123对应平时分6 ，总分=22.16
        studentList.add(student1);
        studentList.add(student2);

        for (Student student : studentList) {
            int stuNo = student.getStuNo();
            Set<Integer> set = new HashSet<>();
            while (stuNo != 0) {
                set.add(stuNo % 10);
                stuNo /= 10;
            }

            Integer[] integers = set.toArray(new Integer[set.size()]);
            int commonSize = allLine.permute(integers);
            BigDecimal b1 = new BigDecimal(student.getScore());
            BigDecimal b2 = new BigDecimal(Double.valueOf(commonSize));
            student.setScore(b1.add(b2).doubleValue());
        }
        allLine.sorted(studentList);
        List<String> collectAns = studentList.stream().map(Student::getName).collect(Collectors.toList());
        System.out.println(collectAns);
    }
}
