package zhangxinhua.lanqiaoyun.sp3;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/28
 */
public class SoftRemoveItemAns {
    public static void main(String[] args) {
        int total = 100;
        try {
            String params = args[0];
            total = Integer.valueOf(params);
        } catch (Exception e) {

        }
        List<People> list = new ArrayList<>();
        for (int i = 0; i < total; i++) {
            int j = i + 1;
            list.add(new People(j,"" + j));
        }
        for (int i = 0; i < list.size(); i++) {
            People people = list.get(i);
            if (Law.getInstance().leave(people)) {
                list.remove(people);
            }
        }
        String result = "";
        for (People people : list) {
            result += "," + people.getIndex();
        }
        if (!"".equals(result)) {
            result = result.substring(1);
        }
        System.out.println(result);
    }
}
