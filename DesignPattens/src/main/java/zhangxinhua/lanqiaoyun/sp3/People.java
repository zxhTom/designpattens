package zhangxinhua.lanqiaoyun.sp3;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/29
 */
public class People {
    private int index;
    private String name;
    public People(int index,String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
