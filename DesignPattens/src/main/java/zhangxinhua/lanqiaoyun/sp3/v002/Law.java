package zhangxinhua.lanqiaoyun.sp3.v002;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Law {
    private static Law law = new Law();

    private Law() {

    }

    public static Law getInstance() {
        return law;
    }
    public List<DeptSp3> init(DeptSp3 parentDept,int size) {
        if (size > 5) {
            return null;
        }
        List<DeptSp3> deptList = new ArrayList<>();
        int deptNum = (int) (Math.random() * 10+1);
        for (int i = 0; i < deptNum; i++) {
            DeptSp3 deptSp3 = new DeptSp3();
            deptSp3.setDeptNo(random(10000)%1000);
            deptSp3.setDeptName(UUID.randomUUID().toString());
            deptSp3.setParentDept(parentDept);
            deptSp3.setChildren(init(deptSp3,++size));
            deptList.add(deptSp3);
        }
        return deptList;
    }

    private int random(int max) {
        int value = (int) (Math.random() * max + 1);

        return value;
    }
}
