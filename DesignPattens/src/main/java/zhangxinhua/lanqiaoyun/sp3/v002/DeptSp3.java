package zhangxinhua.lanqiaoyun.sp3.v002;

import java.util.List;

/**
 * @author zxhtom
 * 2022/8/8
 */
public class DeptSp3 {
    private int deptNo;
    private String deptName;
    private DeptSp3 parentDept;
    private List<DeptSp3> children;

    public DeptSp3 getParentDept() {
        return parentDept;
    }

    public void setParentDept(DeptSp3 parentDept) {
        this.parentDept = parentDept;
    }

    public int getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(int deptNo) {
        this.deptNo = deptNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public List<DeptSp3> getChildren() {
        return children;
    }

    public void setChildren(List<DeptSp3> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        String result = "";
        if (parentDept == null) {
            result +="\n";
//            result += "parentDept=" + String.format("deptNo=%s,deptName=%s", parentDept.getDeptNo(), parentDept.getDeptName());
        }
        result += "deptNo=" + deptNo +
                ", deptName='" + deptName + '\'';

        if (children != null && children.size() > 0) {
//           result += ", children.size()=" + children.size() ;
            result += "\n    ";
            for (int i = 0; i < children.size(); i++) {
                DeptSp3 child = children.get(i);
                for (int j = 0; j <= i; j++) {

                }
                result += child.toString();
            }
        }
        return result;
    }
}
