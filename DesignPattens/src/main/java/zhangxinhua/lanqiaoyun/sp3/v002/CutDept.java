package zhangxinhua.lanqiaoyun.sp3.v002;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/8/8
 */
public class CutDept {

    public static void main(String[] args) {
        CutDept cutDept = new CutDept();
        List<DeptSp3> init = new CutDept().init();
        System.out.println(init);
        cutDept.cut(init);
        System.out.println(init);
    }

    public void print(List<DeptSp3> list) {
        if (list != null && list.size() > 0) {
            for (DeptSp3 deptSp3 : list) {
                System.out.println(deptSp3.toString());
                print(deptSp3.getChildren());
            }
        }
    }
    public void cut(List<DeptSp3> deptSp3List) {
        //TODO 请在此区域开始全局判断元素并删除符合贪官条件的元素
    }
    private List<DeptSp3> init() {
        List<DeptSp3> list = new ArrayList<>();

        //质数构造
        DeptSp3 deptSp31 = new DeptSp3();
        deptSp31.setDeptNo(7);
        deptSp31.setDeptName("我是质数");
        DeptSp3 deptSp311 = new DeptSp3();
        deptSp311.setDeptNo(118);
        deptSp311.setDeptName("我是良民");
        deptSp311.setParentDept(deptSp31);
        deptSp31.setChildren(new ArrayList<DeptSp3>(){
            {
                add(deptSp311);
            }
        });

        //父节点合法自节点违规

        DeptSp3 deptSp32 = new DeptSp3();
        deptSp32.setDeptNo(114);
        deptSp32.setDeptName("我是正常父节点");
        DeptSp3 deptSp321 = new DeptSp3();
        deptSp321.setDeptNo(153);
        deptSp321.setDeptName("我是水仙花");
        deptSp321.setParentDept(deptSp31);
        deptSp32.setChildren(new ArrayList<DeptSp3>(){
            {
                add(deptSp321);
            }
        });
        list.add(deptSp31);
        list.add(deptSp32);
        return list;
    }
}
