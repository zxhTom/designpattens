package zhangxinhua.lanqiaoyun.sp3.v002;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/8/8
 */
public class UnitTest003Ans {
    public static void main(String[] args) {
        CutDeptAns cutDeptAns = new CutDeptAns();
        CutDept cutDept = new CutDept();
//        List<DeptSp3> init = Law.getInstance().init(null, 1);
        List<DeptSp3> init = new UnitTest003Ans().init();
        List<DeptSp3> ansList = new ArrayList<>();
        List<DeptSp3> clientList = new ArrayList<>();
        ansList.addAll(init);
        clientList.addAll(init);
        cutDeptAns.cut(ansList);
        cutDept.cut(clientList);
        //比较两棵树是否一样
        System.out.println(sameTree(ansList, clientList));
    }

    private static boolean sameTree(List<DeptSp3> sourceTreeList, List<DeptSp3> targetTreeList) {
        List<String> sourceList = getList(sourceTreeList);
        List<String> targetList = getList(targetTreeList);
        if (sourceList.size() != targetList.size()) {
            return false;
        }
        boolean flag=true;
        for (String s : sourceList) {
            if (!targetList.contains(s)) {
                flag=false;
                break;
            }
        }
        return flag;
    }

    private static List<String> getList(List<DeptSp3> treeList) {
        if (treeList == null || treeList.size() == 0) {
            return new ArrayList<>();
        }
        List<String> nameList = new ArrayList<>();
        for (DeptSp3 deptSp3 : treeList) {
            nameList.add(deptSp3.getDeptName());
            nameList.addAll(getList(deptSp3.getChildren()));
        }
        return nameList;
    }
    private List<DeptSp3> init() {
        List<DeptSp3> list = new ArrayList<>();

        //质数构造
        DeptSp3 deptSp31 = new DeptSp3();
        deptSp31.setDeptNo(7);
        deptSp31.setDeptName("我是质数");
        DeptSp3 deptSp311 = new DeptSp3();
        deptSp311.setDeptNo(118);
        deptSp311.setDeptName("我是良民");
        deptSp311.setParentDept(deptSp31);
        deptSp31.setChildren(new ArrayList<DeptSp3>(){
            {
                add(deptSp311);
            }
        });

        //父节点合法自节点违规

        DeptSp3 deptSp32 = new DeptSp3();
        deptSp32.setDeptNo(114);
        deptSp32.setDeptName("我是正常父节点");
        DeptSp3 deptSp321 = new DeptSp3();
        deptSp321.setDeptNo(153);
        deptSp321.setDeptName("我是水仙花");
        deptSp321.setParentDept(deptSp31);
        deptSp32.setChildren(new ArrayList<DeptSp3>(){
            {
                add(deptSp321);
            }
        });
        list.add(deptSp31);
        list.add(deptSp32);
        return list;
    }
}
