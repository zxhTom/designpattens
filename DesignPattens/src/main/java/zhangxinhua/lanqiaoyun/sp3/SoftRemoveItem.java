package zhangxinhua.lanqiaoyun.sp3;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/7/28
 */
public class SoftRemoveItem {
    public static void main(String[] args) {
        List<People> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            int j = i + 1;
            list.add(new People(j,"" + j));
        }
        for (People people : list) {
            if (Law.getInstance().leave(people)) {
                list.remove(people);
            }
        }
    }
}
