package zhangxinhua.lanqiaoyun.sp6;

import java.util.List;

/**
 * @author zxhtom
 */
public class DuolaAmeng {
    public static void main(String[] args) {
        Pocket pocket = new Pocket(3);
        //添加三个道具，达到口袋容量上线
        pocket.put(1,"吸水道具");
        pocket.put(2,"喷火道具");
        pocket.put(3,"变小道具");
        //先从口袋中获取一个道具使用一下
        pocket.get(2);
        //再次添加1个新的道具
        pocket.put(4,"变大道具");
        //4个道具到底哪三个会留下呢？ 上述过程中最后留下的道具顺序应该为3，2，4对应的道具
        List<String> list = pocket.printPocket();
        if (0 == list.size()) {
            System.out.println("目前我还没有容纳功能....");
        } else {
            System.out.println("正常口袋应有:" + list);
        }
    }
}
