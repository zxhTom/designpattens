package zhangxinhua.lanqiaoyun.sp6;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxhtom
 */
public class Pocket {
    //口袋最大容量 capacity=10 表示做多可以容纳10个道具
    private int capacity;
    public Pocket(int capacity) {
        //TODO 请你完成口袋初始化工作
        this.capacity = capacity;
    }
    public String get(int key) {
        //TODO 请你完成通过别名获取道具的逻辑
        return null;
    }

    public void put(int key, String value) {
        //TODO 请你完成道具添加到口袋的逻辑
    }

    public List<String> printPocket() {
        //TODO 请你打印出口袋中目前容纳的道具名称吧。即put函数中的value集合不需要按照一定的顺序
        return new ArrayList<>();
    }
}
