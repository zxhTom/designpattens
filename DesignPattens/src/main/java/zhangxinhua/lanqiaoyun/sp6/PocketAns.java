package zhangxinhua.lanqiaoyun.sp6;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zxhtom
 */
public class PocketAns extends LinkedHashMap<Integer, String> {
    private int capacity;

    public PocketAns(int capacity) {
        super(capacity, 0.75F, true);
        this.capacity = capacity;
    }

    public String get(int key) {
        return super.getOrDefault(key, null);
    }

    public void put(int key, String value) {
        super.put(key, value);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<Integer, String> eldest) {
        return size() > capacity;
    }

    public List<String> printPocket() {
        return super.values().stream().collect(Collectors.toList());
    }
}
