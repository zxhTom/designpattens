package zhangxinhua.lanqiaoyun.sp6;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author zxhtom
 */
public class PocketTest {
    public static void main(String[] args) {
        List<String> lineList = new ArrayList<>();
        lineList.add("乐高");
        lineList.add("芭比娃娃");
        lineList.add("魔方");
        lineList.add("拼图");
        lineList.add("纸牌变形金刚");
        lineList.add("弹弓");
        lineList.add("陀螺");
        lineList.add("不倒翁");
        lineList.add("溜溜球");
        lineList.add("俄罗斯方块机");
        lineList.add("芭比娃娃");
        lineList.add("泰迪熊");
        lineList.add("兔斯基");
        lineList.add("海绵宝宝");
        lineList.add("陀螺");
        lineList.add("积木");
        lineList.add("花园宝宝");
        lineList.add("米菲");
        lineList.add("喜羊羊");
        lineList.add("弹弓");
        lineList.add("小汽车");
        lineList.add("挖掘机");
        Random random = new Random();
        //生成1-5随机数，作为口袋初始容量
        int capacity = random.nextInt(5)+1;
        Pocket pocket = new Pocket(capacity);
        PocketAns pocketAns = new PocketAns(capacity);
        System.out.println("初始化口袋容量="+capacity);
        List<Integer> keyList = new ArrayList<>();
        //超出两次
        System.out.println("开始向口袋中添加或者获取道具............");
        for (int i = 0; i < capacity+2; i++) {
            if (i == capacity) {
                //执行获取
                int keyIndex = random.nextInt(keyList.size());
                pocket.get(keyList.get(keyIndex));
                pocketAns.get(keyList.get(keyIndex));
                System.out.println("pocket.get:"+keyList.get(keyIndex));
            }
            int capacityNum = random.nextInt(100) + 1;
            int capacityContentNum = random.nextInt(lineList.size());
            String capacityContent = lineList.get(capacityContentNum);
            lineList.remove(capacityContentNum);
            keyList.add(capacityNum);
            pocket.put(capacityNum, capacityContent);
            pocketAns.put(capacityNum, capacityContent);
            System.out.println("pocket.put:[" + capacityNum + "," + capacityContent + "]");
        }
        List<String> pocketList = pocket.printPocket();
        if (null == pocketList || pocketList.size() == 0) {
            System.out.println("正常口袋应有:"+pocketAns.printPocket());
            throw new RuntimeException("您的口袋貌似空空如也......");
        }
        List<String> pocketAnsList = pocketAns.printPocket();
        for (String pocketName : pocketList) {
            if (!pocketAnsList.contains(pocketName)) {
                System.out.println("正常口袋应有:"+pocketAns.printPocket());
                System.out.println("你的口袋有:"+pocket.printPocket());
                throw new RuntimeException("您的口袋貌似少了点东西");
            }
        }
    }
}
