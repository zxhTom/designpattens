package zhangxinhua.lanqiaoyun;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/6/16
 */
public class StringDemo {
    public static void main(String[] args) {
        String text = "abc";
        String pre = "123";
        String result = new String(text+pre);
//        String result = new String("sdf");
        System.out.println("abc123:"+System.identityHashCode(result));
//        String re2=result.intern();
//        System.out.println(re2==result);
        result = "new";
        System.out.println("改变了："+result.hashCode());
        String newResult = text+pre;
        newResult=newResult.intern();
        System.out.println("abc123:"+System.identityHashCode(newResult));
        System.out.println("原始:"+System.identityHashCode(newResult));
        System.out.println("@@@@@@@@@");
        String z1=new String("zxhtom");
        String z2=z1.intern();
//        String z3=new String("zxhtom");
        System.out.println( z1==z1.intern() );
        System.out.println( System.identityHashCode(z1)+" "+System.identityHashCode(z2) );
        System.out.println( z2==z1 );
        System.out.println( z2==z1.intern() );
        System.out.println("--------------------------");
        String first = "a";
        String second = "a";
        System.out.println(System.identityHashCode(first) + " " + System.identityHashCode(second));
        System.out.println(first==second);
        second = "c";
        System.out.println(System.identityHashCode(first) + " " + System.identityHashCode(second));
        System.out.println(first==second);

    }
}
