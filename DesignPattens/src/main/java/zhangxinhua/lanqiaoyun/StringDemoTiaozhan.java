package zhangxinhua.lanqiaoyun;

/**
 * @author zxhtom
 * 2022/6/16
 */
public class StringDemoTiaozhan {
    public static void main(String[] args) {
        String text = args[0];
        String pre = args[1];
        String save = args[2];
        String result = new String(text+pre);
        int first = System.identityHashCode(result);
        if (Boolean.valueOf(save)) {
          result.intern();
        }
        int second = Practice.telnet(text,pre);
        System.out.println(first==second);
    }

    public static int telnet(String text, String pre) {
        String newResult = text + pre;
        int psecond = System.identityHashCode(newResult);
        newResult=newResult.intern();
        int second = System.identityHashCode(newResult);
        if (psecond != second) {
            return second;
        }
        return -1;
    }
}
