package zhangxinhua.lanqiaoyun.sp5;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @author zxhtom
 */
public class ThreadLineAns {
    public int i =0;
    public List<Semaphore> semaphoreList = new ArrayList(){
        {
            add(new Semaphore(1));
            add(new Semaphore(0));
            add(new Semaphore(0));
        }
    };
    public static void main(String[] args) {
        new ThreadLineAns().Print();
    }
    public void Print() {
        //创建一个可缓存线程池
        ExecutorService exe = Executors.newCachedThreadPool();
        exe.execute(getThread(9));
        exe.execute(getThread(20));
        exe.execute(getThread(3));
        exe.shutdown();
    }

    public Thread getThread(int index) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int li=i;
                    i++;
                    Semaphore semaphore = semaphoreList.get(li);
                    semaphore.acquire();
                    PrintUtils.getInstance().xixiPrint(index);
                    semaphoreList.get((li+1)%3).release();
                } catch (Exception e) {
                    System.out.println(index + "空空如也");
                }
            }
        });
    }
}
