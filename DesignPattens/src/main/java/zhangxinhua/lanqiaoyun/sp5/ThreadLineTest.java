package zhangxinhua.lanqiaoyun.sp5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zxhtom
 */
public class ThreadLineTest {
    public static void main(String[] args) {
        ThreadLine threadLine = new ThreadLine();
        ExecutorService exe = Executors.newCachedThreadPool();
        exe.execute(threadLine.getThread(Integer.parseInt(args[0])));
        exe.execute(threadLine.getThread(Integer.parseInt(args[1])));
        exe.execute(threadLine.getThread(Integer.parseInt(args[2])));
        exe.shutdown();
    }
}
