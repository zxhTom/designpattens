package zhangxinhua.lanqiaoyun.sp5;

/**
 * @author zxhtom
 */
public class PrintUtils {
    private static PrintUtils utils = new PrintUtils();

    public static PrintUtils getInstance() {
        return utils;
    }
    private PrintUtils() {

    }

    public void xixiPrint(int index) {
        if ("main".equals(Thread.currentThread().getName())) {
            throw new RuntimeException("请在多线程方式输出序列");
        }
        System.out.print(index+",");
    }
}
