package zhangxinhua.lanqiaoyun.sp5;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @author zxhtom
 */
public class ThreadLine {

    public static void main(String[] args) {
        new ThreadLine().Print();
    }
    public void Print() {
        //创建一个可缓存线程池
        ExecutorService exe = Executors.newCachedThreadPool();
        exe.execute(getThread(1));
        exe.execute(getThread(2));
        exe.execute(getThread(3));
        exe.shutdown();
    }


    public Thread getThread(int index) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PrintUtils.getInstance().xixiPrint(index);
            }
        });
    }
}
