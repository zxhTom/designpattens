package zhangxinhua.lanqiaoyun.sp7;

/**
 * @author zxhtom
 */
public class PasswordAns {
    private String msg;
    ThreadLocal<String> msgLocal = new ThreadLocal<>();
    public String getMsg() {
        //TODO 请你完成数据读档 - 线程读取当本线程所存档的数据
        String s = msgLocal.get();
        if (null != s && !"".equals(s)) {
            return s;
        }
        return msg;
    }

    public void setMsg(String msg) {
        //TODO 请你完成数据存档 - 线程将本线程数据存档
        this.msg = msg;
        msgLocal.remove();
        msgLocal.set(msg);
    }
}
