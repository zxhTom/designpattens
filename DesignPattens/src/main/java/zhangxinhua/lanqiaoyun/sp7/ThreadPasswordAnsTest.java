package zhangxinhua.lanqiaoyun.sp7;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author zxhtom
 */
public class ThreadPasswordAnsTest {
    public static List<Thread> threadList = new ArrayList<>();
    public static PasswordBookAns pb = new PasswordBookAns();
    public static PasswordAns password = new PasswordAns();
    public static volatile boolean flag = true;
    public static void main(String[] args) {
        //加密后报文
        String info = "i am lanqiaoyun";
        List<String> keyList = new ArrayList<>();
        for (int j = 0; j < 20; j++) {
            int finalJ = j;
            String initpasswordBook = passwordBook();
            System.out.println(String.format("第%s号线程获取的密码本为：%s", j, initpasswordBook));
            keyList.add(initpasswordBook);
            Thread thread = new Thread(new Runnable() {
                String passwordBook = initpasswordBook;
                @Override
                public void run() {
                    //将解析的报文上传存档
                    password.setMsg(pb.decodeMessage(passwordBook,info));
                    try {
                        Thread.sleep(300); //休眠300毫秒处理其他业务
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //读取本线程档案中上传的解析报文，即上一步setMsg中的内容
                    System.out.println(String.format("我是第%s号线程,破译的报文为：%s",finalJ,password.getMsg()));
                    String initKey = keyList.get(finalJ);
                    String s = pb.decodeMessage(initKey, info);
                    synchronized (this) {
                        if (flag && !s.equals(password.getMsg())) {
                            flag = false;
                            throw new RuntimeException("检测到线程解析报文好像被篡改了....");
                        }
                    }

                }
            });
            //线程加入集合，批量启动
            threadList.add(thread);
        }
        //启动线程
        for (Thread thread : threadList) {
            thread.start();
        }
    }
    public static String passwordBook() {
        StringBuilder passbook = new StringBuilder("abcdefghijklmnopqrstuvwxyz");
        Random random = new Random();
        int addNum = random.nextInt(20);
        for (int j = 0; j < addNum; j++) {
            int insertPosition = random.nextInt(passbook.length());
            int searchPosition = random.nextInt(passbook.length());
            passbook.insert(insertPosition, passbook.charAt(searchPosition));
        }
        return passbook.toString();
    }
}
