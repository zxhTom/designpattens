package zhangxinhua.lanqiaoyun.sp7;


import java.util.HashMap;

/**
 * @author zxhtom
 */
public class PasswordBookAns {

    public String decodeMessage(String key, String message) {
        HashMap<Character, Character> map = new HashMap<Character, Character>();
        for (int i = 0; i < key.length() && map.size() < 26; ++i) {
            char c = key.charAt(i);
            if (map.containsKey(c) || c == ' ') {
                continue;
            }
            map.put(c, (char)('a' + map.size()));
        }
        StringBuffer sb = new StringBuffer();
        for (char c : message.toCharArray()) {
            if (c == ' ') {
                sb.append(' ');
                continue;
            }
            sb.append(map.get(c));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        PasswordBookAns pba = new PasswordBookAns();
        String key = "";
        String message = "i am lanqiaoyun";

        System.out.println(pba.decodeMessage(key, message));
        System.out.println(pba.decodeMessage("abcdefghijklmycnopqrstuvqwxyz", message));
        System.out.println(pba.decodeMessage("aqbcdefghifjklrmknopqrstbiuvwvxyz", message));
        System.out.println(pba.decodeMessage("aubcvztdefgheiijklmlnkohpqirstuvwjhxfyz", message));
    }
}
