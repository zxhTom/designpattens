package zhangxinhua.JavaDesignDetails.访问者模式;

import zhangxinhua.JavaDesignDetails.components.model.Feman;
import zhangxinhua.JavaDesignDetails.components.model.Man;

/**
 * @package zhangxinhua.JavaDesignDetails.访问者模式
 * @Class IVisitor
 * @Description 抽象访问者角色
 * @Author zhangxinhua
 * @Date 19-10-17 上午10:08
 */
public interface IVisitor {

    public void accept(Feman feman);

    public void accept(Man man);
}
