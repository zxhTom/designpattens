package zhangxinhua.JavaDesignDetails.访问者模式;

import zhangxinhua.JavaDesignDetails.components.model.Feman;
import zhangxinhua.JavaDesignDetails.components.model.Man;

/**
 * @package zhangxinhua.JavaDesignDetails.访问者模式
 * @Class Visit
 * @Description 具体访问者１
 * @Author zhangxinhua
 * @Date 19-10-17 上午11:06
 */
public class Visit implements IVisitor {

    @Override
    public void accept(Feman feman) {
        System.out.println(feman.getSex() + ":执行相关操作");
    }

    @Override
    public void accept(Man man) {
        System.out.println(man.getSex() + ":执行相关操作");
    }
}
