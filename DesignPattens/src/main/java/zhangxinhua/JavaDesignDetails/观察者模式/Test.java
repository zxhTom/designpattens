package zhangxinhua.JavaDesignDetails.观察者模式;

import zhangxinhua.JavaDesignDetails.观察者模式.client.UserOne;
import zhangxinhua.JavaDesignDetails.观察者模式.service.Subject;
import zhangxinhua.JavaDesignDetails.观察者模式.service.WeChatSubject;

public class Test {
    public static void main(String[] args) {
        UserOne one = new UserOne();
        UserOne two = new UserOne();
        Subject webChat = new WeChatSubject();
        webChat.registerObserver(one);
        webChat.registerObserver(two);
        webChat.notifyObserver("观察者模式微信主题发布消息了");
        webChat.removeObserver(one);
        webChat.notifyObserver("观察者模式微信主题发布消息2");
    }
}
