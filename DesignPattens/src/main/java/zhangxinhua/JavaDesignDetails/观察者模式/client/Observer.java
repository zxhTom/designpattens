package zhangxinhua.JavaDesignDetails.观察者模式.client;

/**
 * 观察者接口
 */
public interface Observer {
    public void update(Object obj);
}
