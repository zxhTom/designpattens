package zhangxinhua.JavaDesignDetails.观察者模式.client;

import java.util.UUID;

public class UserOne implements Observer {
    private String uuid = UUID.randomUUID().toString();
    @Override
    public void update(Object obj) {
        System.out.println("我是用户("+uuid+")：接收到信息：" + obj);
    }
}
