package zhangxinhua.JavaDesignDetails.观察者模式.service;

import zhangxinhua.JavaDesignDetails.观察者模式.client.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 主题
 * 服务端
 * 被观察者
 * 用来管理管理者，实现通知观察者
 */
public interface Subject {
    public List<Observer> observerList = new ArrayList<>();

    /**
     * 注册观察者
     * @param observer
     */
    default void registerObserver(Observer observer){
        observerList.add(observer);
    }

    /**
     * 删除观察者
     * @param observer
     */
    default void removeObserver(Observer observer){
        observerList.remove(observer);
    }

    /**
     * 发布消息
     * @param object
     */
    default void notifyObserver(Object object){
        for (Observer observer : observerList) {
            observer.update(object);
        }
    }
}
