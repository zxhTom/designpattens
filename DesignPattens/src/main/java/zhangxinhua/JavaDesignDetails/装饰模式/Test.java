package zhangxinhua.JavaDesignDetails.装饰模式;


import zhangxinhua.JavaDesignDetails.装饰模式.装饰.UpCapDecorate;
import zhangxinhua.JavaDesignDetails.装饰模式.装饰.UpClothDecorate;

public class Test
{
    public static void main(String[] args) 
    {
        Human zxh = new Person();
        Decorate decorate = new UpClothDecorate(new UpCapDecorate(zxh));
        decorate.decorate();
        System.out.println(decorate.approve("请求认证是否通过？"));
    }
}
