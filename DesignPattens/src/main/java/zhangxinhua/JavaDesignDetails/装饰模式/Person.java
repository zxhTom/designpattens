package zhangxinhua.JavaDesignDetails.装饰模式;


//具体类的方法功能
public class Person implements Human
{

    @Override
    public void eat()
    {
        System.out.println("张新华喜欢吃烧烤");
        
    }

    @Override
    public void decorate()
    {
        System.out.println("张新华在穿袜子");
        
    }

    @Override
    public String approve(String approve)
    {
        approve+="_张新华";
        return approve;
    }

}
