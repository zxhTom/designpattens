package zhangxinhua.JavaDesignDetails.装饰模式;

public interface Human
{
    //吃
    public void eat();
    
    //穿
    public void decorate();
    
    //认可
    public String approve(String approve);
}
