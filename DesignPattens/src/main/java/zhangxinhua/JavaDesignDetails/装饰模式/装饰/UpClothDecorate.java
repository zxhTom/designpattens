package zhangxinhua.JavaDesignDetails.装饰模式.装饰;

import zhangxinhua.JavaDesignDetails.装饰模式.Decorate;
import zhangxinhua.JavaDesignDetails.装饰模式.Human;

//装饰穿上衣
public class UpClothDecorate extends Decorate
{

    public UpClothDecorate(Human human)
    {
        super(human);
    }

    private void eatBeer()
    {
        System.out.println("吃Beer");
    }
    
    private void holdCloth()
    {
        System.out.println("穿上衣");
    }

    @Override
    public void eat()
    {
        super.eat();
        eatBeer();
    }

    @Override
    public void decorate()
    {
        super.decorate();
        holdCloth();
    }

    @Override
    public String approve(String approve)
    {
        return super.approve(approve)+"_服装协会";
    }

}
