package zhangxinhua.JavaDesignDetails.装饰模式.装饰;

import zhangxinhua.JavaDesignDetails.装饰模式.Decorate;
import zhangxinhua.JavaDesignDetails.装饰模式.Human;

//装饰穿上衣
public class UpCapDecorate extends Decorate
{

    public UpCapDecorate(Human human)
    {
        super(human);
    }

    private void eatBeef()
    {
        System.out.println("吃Beef");
    }
    
    private void holdCap()
    {
        System.out.println("带上帽子");
    }

    @Override
    public void eat()
    {
        super.eat();
        eatBeef();
    }

    @Override
    public void decorate()
    {
        super.decorate();
        holdCap();
    }

    @Override
    public String approve(String approve)
    {
        return super.approve(approve)+"_帽子协会";
    }

}
