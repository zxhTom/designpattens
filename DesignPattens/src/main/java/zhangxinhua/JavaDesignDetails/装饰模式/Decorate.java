package zhangxinhua.JavaDesignDetails.装饰模式;

public abstract class Decorate implements Human
{

    private Human human;

    public Decorate(Human human)
    {
        this.human = human;
    }

    @Override
    public void eat()
    {
        human.eat();

    }

    @Override
    public void decorate()
    {
        human.decorate();
    }

    @Override
    public String approve(String approve)
    {
        return human.approve(approve);
    }
    
    
}
