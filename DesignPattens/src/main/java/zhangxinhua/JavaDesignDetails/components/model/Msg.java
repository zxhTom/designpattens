package zhangxinhua.JavaDesignDetails.components.model;

import lombok.Data;

/**
 * @package zhangxinhua.JavaDesignDetails.components.model
 * @Class Msg
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:31
 */
@Data
public class Msg<T> {
    private String publisher;
    private T msg;
    public Msg(String publisher, T msg) {
        this.publisher = publisher;
        this.msg = msg;
    }
}
