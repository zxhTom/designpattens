package zhangxinhua.JavaDesignDetails.components.model;

/**
 * @package zhangxinhua.JavaDesignDetails.components.model
 * @Class Man
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-10-17 上午10:10
 */
public class Man {
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
