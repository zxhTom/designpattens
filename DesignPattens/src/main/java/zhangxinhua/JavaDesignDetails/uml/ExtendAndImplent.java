package zhangxinhua.JavaDesignDetails.uml;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/5/6
 */
public class ExtendAndImplent {
}
abstract class Happy{
    abstract void sayHi();
}

class TodayHappy extends Happy {

    @Override
    void sayHi() {
        System.out.println("我今天很开心");
    }
}

interface Operator{
    public void opt();
}

class AddOperator implements Operator {

    @Override
    public void opt() {
        System.out.println("我会加法");
    }
}
