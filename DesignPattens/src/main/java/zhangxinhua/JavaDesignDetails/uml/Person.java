package zhangxinhua.JavaDesignDetails.uml;

/**
 * @author zxhtom
 * 2022/5/5
 */
public class Person {
    private Integer id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
