package zhangxinhua.JavaDesignDetails.发布订阅者模式;

import zhangxinhua.JavaDesignDetails.components.model.SubscribePublish;

/**
 * @package zhangxinhua.JavaDesignDetails.发布订阅者模式
 * @Class ISubscribe
 * @Description 订阅者接口
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:28
 */
public interface ISubscribe<T> {

    public void subcribe(SubscribePublish subscribePublish);

    public void unSubcribe(SubscribePublish subscribePublish);

    public void update(String publisher, T message);
}
