package zhangxinhua.JavaDesignDetails.发布订阅者模式;

import zhangxinhua.JavaDesignDetails.components.model.SubscribePublish;
import zhangxinhua.JavaDesignDetails.发布订阅者模式.impl.PublisherImpOne;
import zhangxinhua.JavaDesignDetails.发布订阅者模式.impl.SubcriberImpOne;

/**
 * @package zhangxinhua.JavaDesignDetails.发布订阅者模式.impl
 * @Class Test
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:39
 */
public class SubPubTest {
    public static void main(String[] args) {
        SubscribePublish<String> subscribePublish = new SubscribePublish<String>("订阅器");
        IPublisher<String> publisher1 = new PublisherImpOne<String>("发布者1");
        ISubscribe<String> subcriber1 = new SubcriberImpOne<String>("订阅者1");
        ISubscribe<String> subcriber2 = new SubcriberImpOne<String>("订阅者2");
        subcriber1.subcribe(subscribePublish);
        subcriber2.subcribe(subscribePublish);
        publisher1.publish(subscribePublish, "welcome", true);
        publisher1.publish(subscribePublish, "to", true);
        for (int i = 0; i < 21; i++) {
            publisher1.publish(subscribePublish, "yy"+i, false);
        }
    }
}
