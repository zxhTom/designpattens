package zhangxinhua.JavaDesignDetails.发布订阅者模式;

import zhangxinhua.JavaDesignDetails.components.model.SubscribePublish;

/**
 * @package zhangxinhua.JavaDesignDetails.发布订阅者模式
 * @Class IPublisher
 * @Description 发布者接口
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:26
 */
public interface IPublisher<T> {
    public void publish(SubscribePublish subscribePublish, T message, boolean isInstantMsg);
}
