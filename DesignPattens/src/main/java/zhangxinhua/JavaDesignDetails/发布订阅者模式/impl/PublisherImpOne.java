package zhangxinhua.JavaDesignDetails.发布订阅者模式.impl;

import zhangxinhua.JavaDesignDetails.components.model.SubscribePublish;
import zhangxinhua.JavaDesignDetails.发布订阅者模式.IPublisher;

/**
 * @package zhangxinhua.JavaDesignDetails.发布订阅者模式.impl
 * @Class Publisher
 * @Description 真正的发布者
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:27
 */
public class PublisherImpOne<T> implements IPublisher<T> {
    private String name;

    public PublisherImpOne(String name) {
        super();
        this.name = name;
    }

    public void publish(SubscribePublish subscribePublish, T message, boolean isInstantMsg) {
        subscribePublish.publish(this.name, message, isInstantMsg);
    }
}
