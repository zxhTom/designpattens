package zhangxinhua.JavaDesignDetails.发布订阅者模式.impl;

import zhangxinhua.JavaDesignDetails.components.model.SubscribePublish;
import zhangxinhua.JavaDesignDetails.发布订阅者模式.ISubscribe;

/**
 * @package zhangxinhua.JavaDesignDetails.发布订阅者模式.impl
 * @Class SubcriberImpOne
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-10-24 下午2:34
 */
public class SubcriberImpOne<T> implements ISubscribe<T> {
    public String name;

    public SubcriberImpOne(String name) {
        super();
        this.name = name;
    }

    public void subcribe(SubscribePublish subscribePublish) {
        subscribePublish.subcribe(this);
    }

    public void unSubcribe(SubscribePublish subscribePublish) {
        subscribePublish.unSubcribe(this);
    }

    public void update(String publisher, T message) {
        System.out.println(this.name + "收到" + publisher + "发来的消息:" + message.toString());
    }
}
