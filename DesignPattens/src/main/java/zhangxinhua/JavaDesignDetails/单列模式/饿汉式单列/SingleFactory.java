package zhangxinhua.JavaDesignDetails.单列模式.饿汉式单列;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class SingleFactory
{
    private static Person person = new Person();

    private SingleFactory()
    {
    }

    public static Person getInstance()
    {
        return person;
    }
}
