饿汉式特点是在获取实例之前就创建好实例对象了。
因为是private static Person person = new Person();
static静态块特点是在项目启动运行一次，之后也不会运行。所以
在饿汉式中不会存在线程安全不安全的问题。这里不用担心线程