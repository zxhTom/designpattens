package zhangxinhua.JavaDesignDetails.单列模式.饿汉式单列;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class client
{

    public static void main(String[] args)
    {
        System.out.println("获取第一个Person类：");
        Person person1=SingleFactory.getInstance();
        System.out.println(person1.toString());
        System.out.println("**************************");
        System.out.println("获取第二个Person类：");
        Person person2=SingleFactory.getInstance();
        System.out.println(person2.toString());
    }
}
