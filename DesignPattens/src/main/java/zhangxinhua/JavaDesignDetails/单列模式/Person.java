package zhangxinhua.JavaDesignDetails.单列模式;

import java.util.Date;

public class Person
{
    private int id;
    private String name;
    private Date birth;
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public Date getBirth()
    {
        return birth;
    }
    public void setBirth(Date birth)
    {
        this.birth = birth;
    }
    
    public void sayHello(){
        System.out.println("大家好！！");
    }
    
    
}
