package zhangxinhua.JavaDesignDetails.单列模式.普通单列模式.非线程;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class SingleFactory
{
    private static Person person;

    private SingleFactory()
    {
    }

    public static Person getInstance()
    {
        try
        {
            Thread.sleep(30);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (person == null)
        {
            person = new Person();
        }
        return person;
    }
}
