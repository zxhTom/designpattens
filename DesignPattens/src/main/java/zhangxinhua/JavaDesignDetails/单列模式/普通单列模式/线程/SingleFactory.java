package zhangxinhua.JavaDesignDetails.单列模式.普通单列模式.线程;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class SingleFactory
{
    private static Person person;

    private SingleFactory()
    {
    }

    public synchronized static Person getInstance()
    {
        try
        {
            Thread.sleep(30);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        if (person == null)
        {
            person = new Person();
        }
        return person;
    }
}
