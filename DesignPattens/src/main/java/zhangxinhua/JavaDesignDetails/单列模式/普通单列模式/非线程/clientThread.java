package zhangxinhua.JavaDesignDetails.单列模式.普通单列模式.非线程;


public class clientThread extends Thread
{

    @Override
    public void run()
    {
        System.out.println(SingleFactory.getInstance().toString());
    }
    public static void main(String[] args)
    {
        clientThread[] clients = new clientThread[10];
        for (int i = 0; i < clients.length; i++)
        {
            clients[i]=new clientThread();
        }
        for (int i = 0; i < clients.length; i++)
        {
            clients[i].start();
        }
    }
}
