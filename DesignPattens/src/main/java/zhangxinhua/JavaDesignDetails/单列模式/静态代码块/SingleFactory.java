package zhangxinhua.JavaDesignDetails.单列模式.静态代码块;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/5/9
 */
public class SingleFactory
{
    private static Person person ;

    static {
        person = new Person();
    }

    private SingleFactory()
    {
    }

    public static Person getInstance()
    {
        return person;
    }
}
