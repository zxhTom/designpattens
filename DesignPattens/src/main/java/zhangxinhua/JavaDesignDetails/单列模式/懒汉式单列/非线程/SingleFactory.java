package zhangxinhua.JavaDesignDetails.单列模式.懒汉式单列.非线程;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class SingleFactory
{
    private static Person person = null;

    private SingleFactory()
    {
    }

    public static Person getInstance()
    {
        try
        {
            Thread.sleep(30);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(person==null){
            person=new Person();
        }
        return person;
    }
}
