package zhangxinhua.JavaDesignDetails.单列模式.懒汉式单列.双检查锁机制线程安全;

import zhangxinhua.JavaDesignDetails.单列模式.Person;

public class SingleFactory
{
    volatile private static Person person = null;

    private SingleFactory()
    {
    }

    public static Person getIstance()
    {
        try
        {
            Thread.sleep(30);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        synchronized (SingleFactory.class)
        {
            if (person == null)
            {
                person = new Person();
            }
        }
        return person;
    }
}
