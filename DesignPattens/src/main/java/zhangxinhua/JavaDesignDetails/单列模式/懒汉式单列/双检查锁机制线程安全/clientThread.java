package zhangxinhua.JavaDesignDetails.单列模式.懒汉式单列.双检查锁机制线程安全;


public class clientThread extends Thread
{

    @Override
    public void run()
    {
        System.out.println(SingleFactory.getIstance().toString());
    }

    public static void main(String[] args)
    {
        clientThread[] clients = new clientThread[10];
        for (int i = 0; i < clients.length; i++)
        {
            clients[i] = new clientThread();
        }
        for (int i = 0; i < clients.length; i++)
        {
            clients[i].start();
        }
    }
}
