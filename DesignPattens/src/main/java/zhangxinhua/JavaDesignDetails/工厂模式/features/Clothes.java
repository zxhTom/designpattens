package zhangxinhua.JavaDesignDetails.工厂模式.features;

public abstract class Clothes
{
    //袖口的数量
    private int wristband=2;
    //领口的数量
    private int neckline=1;
    //穿衣方式
    public void hold()
    {
        System.out.println("我的穿衣方式是从领口");
    }
    public int getWristband()
    {
        return wristband;
    }
    public void setWristband(int wristband)
    {
        this.wristband = wristband;
    }
    public int getNeckline()
    {
        return neckline;
    }
    public void setNeckline(int neckline)
    {
        this.neckline = neckline;
    }
    
    
}
