package zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.factory;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;

public interface ClothFactory
{
    public abstract Clothes createClothes();
}
