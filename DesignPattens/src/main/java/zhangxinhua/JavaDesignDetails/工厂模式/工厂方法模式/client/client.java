package zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.client;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;
import zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.factory.ClothFactory;
import zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.factory.SpringClothFactory;

public class client
{
    public static void main(String[] args)
    {
        //需要春装，且知道春装是SpringClothFactory工厂生产的
        ClothFactory clothFactory=new SpringClothFactory();
        Clothes clothes = clothFactory.createClothes();
        clothes.hold();
    }
}
