package zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.factory;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;
import zhangxinhua.JavaDesignDetails.工厂模式.special.AutumnCloth;

public class AutumnClothFactory implements ClothFactory
{

    @Override
    public Clothes createClothes()
    {
        Clothes clothes=null;
        clothes=new AutumnCloth();
        return clothes;
    }

}
