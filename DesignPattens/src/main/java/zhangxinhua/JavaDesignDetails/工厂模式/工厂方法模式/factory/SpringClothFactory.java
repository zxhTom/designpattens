package zhangxinhua.JavaDesignDetails.工厂模式.工厂方法模式.factory;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;
import zhangxinhua.JavaDesignDetails.工厂模式.special.SpringCloth;

public class SpringClothFactory implements ClothFactory
{

    @Override
    public Clothes createClothes()
    {
        Clothes clothes=null;
        clothes=new SpringCloth();
        return clothes;
    }

}
