package zhangxinhua.JavaDesignDetails.工厂模式.简单工厂模式.factory;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;
import zhangxinhua.JavaDesignDetails.工厂模式.special.AutumnCloth;
import zhangxinhua.JavaDesignDetails.工厂模式.special.SpringCloth;
import zhangxinhua.JavaDesignDetails.工厂模式.special.SummerCloth;

public class ClothFactory
{
    public static Clothes createClothes(String type) throws Exception
    {
        Clothes clothes=null;
        switch (type)
        {
        case "spring":
            clothes=new SpringCloth();
            break;
        case "summer":
            clothes=new SummerCloth();
            break;
        case "autumn":
            clothes=new AutumnCloth();
            break;
        default:
            throw new Exception("目前不支持你指定的类型");
        }
        return clothes;
    }
}
