package zhangxinhua.JavaDesignDetails.工厂模式.简单工厂模式.client;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;
import zhangxinhua.JavaDesignDetails.工厂模式.简单工厂模式.factory.ClothFactory;

public class client
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("工厂你给我一套春装");
        Clothes spring = ClothFactory.createClothes("spring");
        spring.hold();
        System.out.println("********************");
        System.out.println("工厂你给我一套秋装");
        Clothes autum = ClothFactory.createClothes("autumn");
        autum.hold();
        System.out.println("********************");
        System.out.println("工厂你给我一套夏装");
        Clothes summer = ClothFactory.createClothes("summer");
        summer.hold();
        System.out.println("********************");
    }
}
