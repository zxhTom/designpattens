package zhangxinhua.JavaDesignDetails.工厂模式.special;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;

public class SpringCloth extends Clothes
{

    @Override
    public void hold()
    {
        System.out.println("我是春装，我要从领口穿衣");
    }

}
