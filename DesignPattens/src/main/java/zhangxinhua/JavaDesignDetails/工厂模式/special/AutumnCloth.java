package zhangxinhua.JavaDesignDetails.工厂模式.special;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;

public class AutumnCloth extends Clothes
{

    @Override
    public void hold()
    {
        System.out.println("我是秋装，我要倒着穿");
    }

}
