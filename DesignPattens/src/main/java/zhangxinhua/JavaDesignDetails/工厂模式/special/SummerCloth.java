package zhangxinhua.JavaDesignDetails.工厂模式.special;

import zhangxinhua.JavaDesignDetails.工厂模式.features.Clothes;

public class SummerCloth extends Clothes
{

    @Override
    public void hold()
    {
        System.out.println("我是夏装，我要从怀口穿");
    }

}
