package zhangxinhua.JavaDesignDetails.设计原则.开闭原则;

import java.awt.*;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/25
 */
public class OpenClose {
    public static void main(String[] args) {
        GraphicEditor editor = new GraphicEditor();
        Shape rectangle = new Rectangle();
        Shape circle = new Circle();
        editor.drawShape(rectangle);
        editor.drawShape(circle);
    }
}

class GraphicEditor{
    public void drawShape(Shape shape) {
        shape.draw();
    }
}
abstract class Shape{
    abstract public void draw();
}

class Rectangle extends Shape {


    @Override
    public void draw() {
        System.out.println("画矩形");
    }
}

class Circle extends Shape {

    @Override
    public void draw() {
        System.out.println("画圆形");
    }
}