package zhangxinhua.JavaDesignDetails.设计原则.里氏替换.improve;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/22
 */
public class LiReplace {
    public static void main(String[] args) {
        Math math = new Math();
        System.out.println("3+4="+math.compute(3,4));
        ZMath zMath = new ZMath();
        System.out.println("3+4="+zMath.compute(3,4));
        System.out.println("13-4="+zMath.sub(13,4));

    }
}

class Base {
    public Integer compute(int a, int b) {
        return a*b;
    }
}
class Math extends Base{
    @Override
    public Integer compute(int a, int b) {
        return a+b;
    }
}

class ZMath extends Base {
    private Math math = new Math();
    @Override
    public Integer compute(int a, int b) {
        return a-b;
    }

    public Integer sub(int a, int b) {
        return math.compute(a,b);
    }
}
