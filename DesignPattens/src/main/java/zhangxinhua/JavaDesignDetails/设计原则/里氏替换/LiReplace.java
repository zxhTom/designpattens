package zhangxinhua.JavaDesignDetails.设计原则.里氏替换;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/22
 */
public class LiReplace {
    public static void main(String[] args) {
        Math math = new Math();
        System.out.println("3+4="+math.add(3,4));
        ZMath zMath = new ZMath();
        System.out.println("3+4="+zMath.add(3,4));
        System.out.println("13-4="+zMath.sub(13,4));

    }
}
class Math{
    public Integer add(int a, int b) {
        return a+b;
    }
}

class ZMath extends Math {
    @Override
    public Integer add(int a, int b) {
        return a-b;
    }

    public Integer sub(int a, int b) {
        return a-b;
    }
}