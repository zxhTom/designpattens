package zhangxinhua.JavaDesignDetails.设计原则.合成复用.improve;

/**
 * @author zxhtom
 * 2022/5/5
 */
public class Hecheng {
    public static void main(String[] args) {
        Color redColor = new Color() {
            @Override
            public String getCurrentColor() {
                return "红色";
            }
        };
        Color whileColor = new Color() {
            @Override
            public String getCurrentColor() {
                return "白色色";
            }
        };
        Color blackColor = new Color() {
            @Override
            public String getCurrentColor() {
                return "黑色";
            }
        };
        PertolCar pertolCar1 = new PertolCar(redColor);
        PertolCar pertolCar2 = new PertolCar(whileColor);
        PertolCar pertolCar3 = new PertolCar(blackColor);
        ElectricCar electricCar1 = new ElectricCar(redColor);
        ElectricCar electricCar2 = new ElectricCar(whileColor);
        ElectricCar electricCar3 = new ElectricCar(blackColor);
        pertolCar1.driver();
        pertolCar2.driver();
        pertolCar3.driver();
        electricCar1.driver();
        electricCar2.driver();
        electricCar3.driver();
    }
}

interface Color{
    public String getCurrentColor();
}

class Car{
    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Car(Color color) {
        this.color = color;
    }
    public void driver() {
        System.out.println(this.color.getCurrentColor()+"我是汽车，可以行驶");
    }
}

class PertolCar extends Car {
    public PertolCar(Color color) {
        super(color);
    }

    @Override
    public void driver() {
        System.out.println(getColor().getCurrentColor()+"我是汽油汽车，正在行驶");
    }
}

class ElectricCar extends Car {
    public ElectricCar(Color color) {
        super(color);
    }

    @Override
    public void driver() {
        System.out.println(getColor().getCurrentColor()+"我是电动汽车，正在行驶");
    }
}

