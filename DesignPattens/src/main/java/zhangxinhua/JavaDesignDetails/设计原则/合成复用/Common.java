package zhangxinhua.JavaDesignDetails.设计原则.合成复用;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/5/2
 */
public class Common {
    public static void main(String[] args) {
        new WhileElectricCar().driver();
        new RedElectricCar().driver();
        new WhilePertolCar().driver();
        new RedPertolCar().driver();
    }
}
class Car{
    public void driver() {
        System.out.println("我是汽车，可以行驶");
    }
}

class PertolCar extends Car {
    @Override
    public void driver() {
        System.out.println("我是汽油汽车，正在行驶");
    }
}

class WhilePertolCar extends PertolCar {
    @Override
    public void driver() {
        System.out.println("我是白色的汽油汽车，正在行驶");
    }
}

class RedPertolCar extends PertolCar {
    @Override
    public void driver() {
        System.out.println("我是红色的汽油汽车，正在行驶");
    }
}
class ElectricCar extends Car {
    @Override
    public void driver() {
        System.out.println("我是电力汽车，正在行驶");
    }
}

class WhileElectricCar extends ElectricCar {
    @Override
    public void driver() {
        System.out.println("我是白色的电力汽车，正在行驶");
    }
}

class RedElectricCar extends ElectricCar {
    @Override
    public void driver() {
        System.out.println("我是红色的电力汽车，正在行驶");
    }
}

