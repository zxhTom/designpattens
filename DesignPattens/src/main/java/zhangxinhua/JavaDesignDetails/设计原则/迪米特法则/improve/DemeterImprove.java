package zhangxinhua.JavaDesignDetails.设计原则.迪米特法则.improve;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/28
 */
public class DemeterImprove {
    public static void main(String[] args) {
        new SchoolManager().printAllEmployers(new CollegeManager());
    }
}

class Employer{
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "collge:"+this.id;
    }
}

class CollegeEmployer{
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "school:"+this.id;
    }
}

class SchoolManager{
    public List<Employer> schoolAllEmployers() {
        List<Employer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Employer employer = new Employer();
            employer.setId(String.valueOf(i));
            list.add(employer);
        }
        return list;
    }

    public void printAllEmployers(CollegeManager manager) {
        /*System.out.println(Arrays.toString(manager.collegeAllEmployers().toArray()));
        System.out.println(Arrays.toString(schoolAllEmployers().toArray()));*/
        manager.printAllCollegeEmployers();
        for (Employer schoolAllEmployer : schoolAllEmployers()) {
            System.out.println(schoolAllEmployer.getId());
        }
    }
}

class CollegeManager{
    public List<CollegeEmployer> collegeAllEmployers() {
        List<CollegeEmployer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            CollegeEmployer collegeEmployer = new CollegeEmployer();
            collegeEmployer.setId(String.valueOf(i));
            list.add(collegeEmployer);
        }
        return list;
    }

    public void printAllCollegeEmployers() {
        for (CollegeEmployer collegeAllEmployer : collegeAllEmployers()) {
            System.out.println(collegeAllEmployer.getId());
        }
    }
}
