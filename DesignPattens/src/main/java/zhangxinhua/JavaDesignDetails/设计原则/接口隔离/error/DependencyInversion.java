package zhangxinhua.JavaDesignDetails.设计原则.接口隔离.error;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/19
 */
public class DependencyInversion {
    public static void main(String[] args) {
        Person person = new Person();
        person.recevice(new Email());
    }
}

class Email {
    public String getInfo() {
        return "电邮";
    }
}

class Person{
    public void recevice(Email email) {
        System.out.println(email.getInfo());
    }
}
