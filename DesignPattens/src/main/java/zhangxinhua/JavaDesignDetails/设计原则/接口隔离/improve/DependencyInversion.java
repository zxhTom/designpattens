package zhangxinhua.JavaDesignDetails.设计原则.接口隔离.improve;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/4/19
 */
public class DependencyInversion {
    public static void main(String[] args) {
        Person person = new Person();
        person.recevice(new Email());
    }
}

interface IReceiver{
    public String getInfo();
}
class Email implements IReceiver{
    @Override
    public String getInfo() {
        return "电邮";
    }
}

class Person{
    public void recevice(IReceiver receiver) {
        System.out.println(receiver.getInfo());
    }
}
