package zhangxinhua.JavaDesignDetails;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author zxhtom
 * 2022/6/18
 */
public class MacTest {
    public static void main(String[] args) {

    }


    public List<String> getMacAddress() throws Exception {

        //
        List<String> result = new ArrayList<>();

        //使用dmidecode命令获取CPU序列号
        String[] shell = {"/bin/bash", "-c", "sudo ifconfig -a | grep 'ether ' | awk '{ print $2}'"};
        Process process = Runtime.getRuntime().exec(shell);
        process.getOutputStream().close();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String line;
        while((line = reader.readLine()) != null) {
            if(null!=line) {
                result.add(line.trim());
            }
        }

        reader.close();
        return result;
    }
}
